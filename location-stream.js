var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var bodyparser = require('body-parser');
app.use(bodyparser.json());

var PORT = 4545;
var HOST = '127.0.0.1';

io.on('connection', function (socket) {
    console.log('a user connected');

    socket.on('join-room', function(room_id){
        socket.join(room_id, function() {
            console.log("user joined the room: " + room_id);
        });
    });

    socket.on('push-message', function(geopoint){
        console.log(geopoint);
        io.sockets.in(geopoint.room_id).emit('location', geopoint.data);
    });

    socket.on('leave-room', function(room_id){
        socket.leave(room_id, function(){
            console.log("user left the room: " + room_id);
        });
    });

    socket.on('disconnect', function(){
        console.log("a user disconnected");
    })
})

app.post('/ride', function(req, res) {
    console.log('receive new ride request');
    io.sockets.in(req.body.stand_id).emit('new-ride');
    res.send('forwarded request to driver');
});

http.listen(PORT, HOST, function(){
    console.log('listening at http://' + HOST + ':' + PORT);
});
